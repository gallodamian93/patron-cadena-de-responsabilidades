
public interface IDevolvedor {
	
	void setSigDevolvedor(IDevolvedor dev);
	void devolverBilletes(int pesos);
}
