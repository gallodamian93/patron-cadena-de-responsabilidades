
public class Test {
	private static IDevolvedor devolvedor;
	
	public static void main(String[] args) {
		encadenar();
		devolvedor.devolverBilletes(19);

	}


	
	private static void encadenar(){
		IDevolvedor dev100 = new Devolvedor100();
		IDevolvedor dev50 = new Devolvedor50();
		IDevolvedor dev20 = new Devolvedor20();
		IDevolvedor dev10 = new Devolvedor10();
		IDevolvedor dev5 = new Devolvedor5();
		IDevolvedor dev1 = new Devolvedor1();
		
		dev100.setSigDevolvedor(dev50);
		dev50.setSigDevolvedor(dev20);
		dev20.setSigDevolvedor(dev10);
		dev10.setSigDevolvedor(dev5);
		dev5.setSigDevolvedor(dev1);
		
		devolvedor = dev100;
	}
	
}
