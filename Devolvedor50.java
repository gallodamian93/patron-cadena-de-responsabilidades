
public class Devolvedor50 implements IDevolvedor{
	IDevolvedor siguiente;
	private static int valor = 50;

	@Override
	public void setSigDevolvedor(IDevolvedor dev) {
		siguiente = dev;
		
	}

	@Override
	public void devolverBilletes(int pesos) {
		int billetes;
		if (pesos >= valor) {
			billetes = pesos/valor;
			pesos = pesos - billetes * valor;
			System.out.println(billetes + " x $" + valor);
		}

		if (pesos > 0) {
			siguiente.devolverBilletes(pesos);
		}
		
	}
}
