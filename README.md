## Sobre el proyecto

El proyecto muestra como funciona el patrón *cadena de responsabilidades*. El ejemplo se basa en un cajero automático que tiene que devolver el monto seleccionado en la menor cantidad de billetes.